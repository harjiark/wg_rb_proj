lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "WG_PROJ"
  spec.version       = '1.0'
  spec.authors       = ["Harji"]
  spec.email         = ["harjiark@gmail.com"]

  spec.files         = ['lib/wgproj.rb']
  spec.executables   = ['bin/wgproj']
  spec.test_files    = ['tests/test_NAME.rb']
  spec.require_paths = ["lib"]
end