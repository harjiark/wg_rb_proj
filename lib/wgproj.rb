require 'rubygems'
require 'nokogiri'
require 'restclient'


class WgProj



	def execute

        # origin 
        o1 = "CGK"
		# destination
		d1 = "KUL"

		# keep it asis
		culture= "en-GB"

		# departure date
		dd1 = "2016-10-18"

		#adult passengger
		adt = "1"
		#child passengger
		chd = "0"

		inl = "0"
		s = "true"
		mon = "true"

		#currency
		cc = "IDR"
		c = "false"

		# https://booking.airasia.com/Flight/Select?o1=CGK&d1=KUL&culture=en-GB&dd1=2016-10-07&ADT=1&CHD=0&inl=0&s=true&mon=true&cc=IDR&c=false

		params = "o1=" + o1 + "&d1=" + d1 + "&culture=" + culture + "&dd1=" + dd1 + "&ADT=" + adt + "&CHD=" + chd + "&inl=" + inl + "&s=" + s + "&mon=" + mon + "&cc=" + cc + "&c=" + c

		req_url = "https://booking.airasia.com/Flight/Select?"+params

		p " Request to "+req_url
		p "please wait ...."

		page = Nokogiri::HTML(RestClient.get(req_url),nil,'UTF-8')do|config|
		config.strict.noblanks
	end   


		# binding.pry

		# page = Nokogiri::HTML.parse(File.open("./resources/exampleresponse.html"),nil,'UTF-8')do|config|
		# 	config.strict.noblanks
		# end   


		table = page.search('table[class="table avail-table"]').first

		# puts page

		# rows = table.css("tr[class~='fare-dark-row|fare-light-row']")

		rows_dark = table.search("tr[class~='fare-dark-row']")
		rows_light = table.search("tr.fare-light-row")
		# rows = table.css('tr').Select{|tr| tr['class'] =~ 'fare-dark-row|fare-light-row' }

		puts "fetching data from airasia"
		puts "rows_dark size=",rows_dark.size
		puts "rows_light size=",rows_light.size

		all_data =rows_light+rows_dark

		puts "all_data size=",all_data.size
		tables_data = []

		flight_list =  Array.new

		p "=========================================="
		p "=========================================="

		lowest_price = 0

		all_data.each do |row|

			duration = row.search("div[class='carrier-hover-oneway-header']").text.gsub(/\n\s+/, "")
			flight_code = row.search("div[class='carrier-hover-oneway-header'] div[class='carrier-hover-bold']").text.gsub(/\n\s+/, "")
			depart_arrived_time = row.search("table[class='avail-table-detail-table'] td[class='avail-table-detail']").text.gsub(/\n\s+/, "")
			price = row.search("td[class$='avail-fare depart LF'] div[class='avail-fare-price-container']").text.gsub(/\n\s+/, "")
			avail_fare_seat = row.search("div[class='avail-fare-seats']").text.gsub(/\n\s+/, " ")

			data_flight_hash =  {}


			if duration.length > 0
				# puts "duration: "
				# puts duration
				data_flight_hash[:flight_duration] = duration.gsub! "\r", ""
			end

			if flight_code.length > 0
				# puts "flight_code: "
				# puts  flight_code

				data_flight_hash[:flight_code] = flight_code.gsub! "\r", ""
			end

			if depart_arrived_time.length > 0
				# puts "depart_arrived_time: "
				# puts depart_arrived_time

				data_flight_hash[:depart_arrived_time] = depart_arrived_time.gsub! "\r", ""				
			end

			if price.length > 0
				# puts "price: "
				# puts  price
				
				lp = price.gsub(/\D/, '')

				data_flight_hash[:price] = lp.to_f
				data_flight_hash[:price_currency] = price.gsub! "\r", ""
			end

			if avail_fare_seat.length > 0
				# puts "avail_fare_seat"
				# puts avail_fare_seat
				data_flight_hash[:avail_fare_seat] = avail_fare_seat.gsub! "\r", ""
			end

			# puts "=========================================="
			# tables_data << [duration, flight_code,depart_arrived_time,price,avail_fare_seat]
			if data_flight_hash.size > 0
				flight_list.push(data_flight_hash)
			end
		end
		

		sort_flight=flight_list.sort_by { |k| k[:price] }

		puts 'the cheapest flight is: '

		p sort_flight.first

		p '==============================='

		p sort_flight
	end

end 

wg = WgProj.new

wg.execute

